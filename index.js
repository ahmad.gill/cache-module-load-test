const express = require('express');
const bodyParser = require('body-parser');
const { cacheInterface } = require('./cacheInterface')
// Create an Express application
const app = express();

// Parse incoming requests with JSON payloads
app.use(bodyParser.json());

// Define a route to handle GET requests to the root URL
app.get('/', (req, res) => {
    res.send('Hello, World!');
});

// Define a route to handle GET requests to /api/users
app.get('/api/users', async (req, res) => {
    // Simulated data - replace with actual data retrieval logic
    const user = [
        { id: 1, name: 'John' },
        { id: 2, name: 'Alice' },
        { id: 3, name: 'Bob' }
    ];

    // await cacheInterface.hset('user', user[0].id, user[0]);

    const result2 = await cacheInterface.getOrset('user', user[0].id, ()=>{return 123});
    console.log("result", result2);
    const result = await cacheInterface.hget('user', user[0].id);
    console.log("result", result);

    const result1 = await cacheInterface.hgetall('user');
    console.log("result1", result1);

    await cacheInterface.delete('user'+user[0].id);


    res.json();
});

// Define a route to handle POST requests to /api/users
app.post('/api/users', (req, res) => {
    // Simulated data - replace with actual data creation logic
    const newUser = {
        id: 4,
        name: req.body.name // Assuming the request body contains a 'name' field
    };

    res.status(201).json(newUser);
});

// Start the server
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
