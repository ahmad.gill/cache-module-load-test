const { CacheInterfaceBuilder } = require('@adnoc-dist/adnoc-cache-module');

const eventModule = {
  KAFKA_CLIENT_LOGLEVEL: 2,
  NAME: 'development',
  BROKERS_LIST: "b-2.devmsk2.3i59ki.c8.kafka.eu-west-1.amazonaws.com:9098,b-1.devmsk2.3i59ki.c8.kafka.eu-west-1.amazonaws.com:9098,b-3.devmsk2.3i59ki.c8.kafka.eu-west-1.amazonaws.com:9098",
  TOPIC_PARITION_COUNT: 2,
  TOPIC_REPLICATION_FACTOR: 2,
  KAFKA_CLIENT_ID: "gitlab"
}

const cache = {
  "CACHE_NODES": [
    {
      "host": "dev-adnocmob-redis-lmpuqz.serverless.euw1.cache.amazonaws.com",
      "port": 6379
    }
  ],
  "CACHE_TYPE": "AWS-ELASTIC-CACHE-CLUSTER",
  "CACHE_OPTIONS": {
    // "scaleReads": "slave",
    "maxConcurrent": 50,
    "minIdleTime": 10000,
    dnsLookup: (address, callback) => callback(null, address),
    redisOptions: {
      tls: {},
    },
  },
  "KAFKA_CONSUMER_GROUP_ID": 'Cache-GITLAB',
  "EVENT_MODULE": eventModule,
  "APP_NAME": "gitlab-app"
}

const cacheInterface = CacheInterfaceBuilder.getCacheInterface(cache);


module.exports = { cacheInterface };